package br.com.treinamento.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MarvelMD5HashBuilderTest
{

    @Test
    public void testValidHash()
    {
        MarvelMD5HashGeneratorService.PRIVATE_KEY = "abcd";
        MarvelMD5HashGeneratorService.PUBLIC_KEY = "1234";
        String hash = new MarvelMD5HashGeneratorService().generate(1l);

        assertEquals("ffd275c5130566a2916217b101f26150", hash);
    }
    
    
}
