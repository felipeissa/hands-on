package br.com.treinamento.entity;

import java.util.Collections;
import java.util.Map;

public class RestCallAddress
{
    private String url;
    private Map<String, Object> templates;
    private Map<String, Object> parameters;

    RestCallAddress(String url)
    {
        this.url = url;
        this.templates = Collections.emptyMap();
        this.parameters = Collections.emptyMap();
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Map<String, Object> getTemplates()
    {
        return templates;
    }

    public void setTemplates(Map<String, Object> templates)
    {
        this.templates = templates;
    }

    public Map<String, Object> getParameters()
    {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters)
    {
        this.parameters = parameters;
    }

}
