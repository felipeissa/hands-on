package br.com.treinamento.entity;

import java.util.HashMap;
import java.util.Map;

public class RestCallAddressBuilder
{

    private String address;
    private Map<String, Object> parameters;
    private Map<String, Object> templates;

    public RestCallAddressBuilder(String address)
    {
        this.address = address;
        this.parameters = new HashMap<>();
        this.templates = new HashMap<>();
    }

    public RestCallAddressBuilder addParameter(String key, Object value)
    {
        parameters.put(key, value);
        return this;
    }

    public RestCallAddressBuilder addTemplate(String key, Object value)
    {
        templates.put(key, value);
        return this;
    }

    public RestCallAddress build()
    {
        RestCallAddress callAddress = new RestCallAddress(address);
        if (!parameters.isEmpty())
        {
            callAddress.setParameters(parameters);
        }
        if (!templates.isEmpty())
        {
            callAddress.setTemplates(templates);
        }
        return callAddress;
    }
}
