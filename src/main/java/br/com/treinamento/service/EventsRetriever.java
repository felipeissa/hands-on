package br.com.treinamento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import br.com.treinamento.entity.RestCallAddressBuilder;

@Service
public class EventsRetriever
{
    private static JsonArray EVENTS;
    
    @Autowired
    RestClientService RestClientService;

    public RestClientService getRestClient()
    {
        return RestClientService;
    }

    public void setRestClient(RestClientService restClient)
    {
        this.RestClientService = restClient;
    }
    
    public JsonArray retrieveEvents()
    {
        if(EVENTS == null)
        {
            JsonObject fullResponse = RestClientService.retrieveData(
                    new RestCallAddressBuilder("https://gateway.marvel.com:443/v1/public/events")
                    .addParameter("limit", Integer.valueOf(100))
                    .build());
            EVENTS = fullResponse.getAsJsonObject("data").getAsJsonArray("results");
            
        }

        return EVENTS;
    }
}
