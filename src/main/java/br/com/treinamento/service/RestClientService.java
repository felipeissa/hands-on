package br.com.treinamento.service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import br.com.treinamento.entity.RestCallAddress;

@Service
public class RestClientService
{
    
    @Autowired
    private MarvelMD5HashGeneratorService hashGeneratorService;

    public JsonObject retrieveData(RestCallAddress callAddress)
    {
        Long currentTime = System.currentTimeMillis();
        Client clientRequest = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
        WebTarget webTarget = clientRequest.target(callAddress.getUrl())
                .queryParam("apikey", "6e1d380424b4fbd0c20b16d173950e19")
                .queryParam("ts", currentTime)
                .queryParam("hash", hashGeneratorService.generate(currentTime));

        if(!callAddress.getParameters().isEmpty())
        {
            for(String key: callAddress.getParameters().keySet())
            {
                webTarget = webTarget.queryParam(key, callAddress.getParameters().get(key));
            }
        }
        
        if(!callAddress.getTemplates().isEmpty())
        {
            for(String key: callAddress.getTemplates().keySet())
            {
                webTarget = webTarget.resolveTemplate(key, callAddress.getTemplates().get(key));
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

        Response responseObject = invocationBuilder.get();
        String stringifiedResponse = responseObject.readEntity(String.class);

        Gson gson = new Gson();
        JsonObject response = gson.fromJson(stringifiedResponse, JsonObject.class);
        
        if(response.get("code").getAsInt() != 200)
        {
            throw new RuntimeException("Failed to retrieve data from server");
        }
        
        return response;
    }

    public MarvelMD5HashGeneratorService getHashGeneratorService()
    {
        return hashGeneratorService;
    }

    public void setHashGeneratorService(MarvelMD5HashGeneratorService hashGeneratorService)
    {
        this.hashGeneratorService = hashGeneratorService;
    }
}
