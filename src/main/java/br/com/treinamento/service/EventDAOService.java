package br.com.treinamento.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;

import br.com.treinamento.entity.RestCallAddressBuilder;

@Service
public class EventDAOService
{

    private Map<String, JsonObject> cache = new HashMap<>();
    @Autowired
    private RestClientService restService;
    
    public RestClientService getRestService()
    {
        return restService;
    }

    public void setRestService(RestClientService restService)
    {
        this.restService = restService;
    }

    public JsonObject getEvent(String itemId)
    {
        if(!cache.containsKey(itemId))
        {
            JsonObject event = restService.retrieveData(
                    new RestCallAddressBuilder("https://gateway.marvel.com:443//v1/public/events/{eventId}")
                    .addTemplate("eventId", itemId)
                    .build()
            );
            event = event.getAsJsonObject("data").getAsJsonArray("results").get(0).getAsJsonObject();
            cache.put(itemId, event);
        }
        return cache.get(itemId);
    }
}
