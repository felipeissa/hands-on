package br.com.treinamento.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Service;

@Service
public class MarvelMD5HashGeneratorService
{
    static String PRIVATE_KEY = "13db3bedf429e35b2b7014f959eda97477d48c76";
    static String PUBLIC_KEY = "6e1d380424b4fbd0c20b16d173950e19";

    /**
     * Generate a MD5 hash for a call to marvel API
     * 
     * The hash is generated with the following patter:
     * 
     *   timeStamp + privateKey + publicKey
     * @param timeStamp
     * @return string representing the md5 hash
     */
    public String generate(Long timeStamp)
    {
        byte[] hash;
        try
        {
            hash = MessageDigest.getInstance("MD5").digest((timeStamp + PRIVATE_KEY + PUBLIC_KEY).getBytes());
        } catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException("Failed to generate hash!", e);
        }
        return new BigInteger(1, hash).toString(16);

    }
}
