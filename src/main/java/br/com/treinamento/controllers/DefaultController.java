package br.com.treinamento.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import br.com.treinamento.service.EventDAOService;
import br.com.treinamento.service.EventsRetriever;

@Controller
public class DefaultController
{

    @Autowired
    EventsRetriever marvelDataRetriever;
    
    @Autowired
    EventDAOService eventDAOService;

    
    public EventsRetriever getMarvelDataRetriever()
    {
        return marvelDataRetriever;
    }


    public void setMarvelDataRetriever(EventsRetriever marvelDataRetriever)
    {
        this.marvelDataRetriever = marvelDataRetriever;
    }


    @RequestMapping("/")
    @ResponseBody
    public ModelAndView home(Map<String, Object> model)
    {
        JsonArray dados = marvelDataRetriever.retrieveEvents();
        model.put("eventos", dados);
        return new ModelAndView("welcome");
    }

    @RequestMapping("/detalhes")
    @ResponseBody
    public ModelAndView save(@RequestParam String itemId, Map<String, Object> model)
    {
        JsonObject event = eventDAOService.getEvent(itemId);
        model.put("evento", event);
        return new ModelAndView("details");
    }
}