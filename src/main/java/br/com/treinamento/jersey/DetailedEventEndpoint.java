package br.com.treinamento.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.treinamento.service.EventDAOService;

@Component
@Path("/detailedEvent")
public class DetailedEventEndpoint {

    @Autowired
    private EventDAOService eventDAOService;
    

    public void setEventDAOService(EventDAOService eventDAOService)
    {
        this.eventDAOService = eventDAOService;
    }


    @GET
    public String datailedEvent(@QueryParam("eventId") String eventId) {
        return eventDAOService.getEvent(eventId).toString();
    }

}
