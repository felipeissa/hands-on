package br.com.treinamento.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.treinamento.service.EventsRetriever;

@Component
@Path("/allevents")
public class AllEventsEndpoint {

    @Autowired
    private EventsRetriever eventsRetriever; 
    
    public void setEventsRetriever(EventsRetriever eventsRetriever)
    {
        this.eventsRetriever = eventsRetriever;
    }

    @GET
    public String allEvents() {
        return eventsRetriever.retrieveEvents().toString();
    }

}
