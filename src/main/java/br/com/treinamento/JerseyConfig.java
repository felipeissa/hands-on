package br.com.treinamento;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import br.com.treinamento.jersey.AllEventsEndpoint;
import br.com.treinamento.jersey.DetailedEventEndpoint;

@Component
@ApplicationPath("/rest")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(AllEventsEndpoint.class);
        register(DetailedEventEndpoint.class);
    }

}
