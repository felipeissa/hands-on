package br.com.treinamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan(basePackages = {"br.com.treinamento", "br.com.treinamento.controllers", "br.com.treinamento.service", "br.com.treinamento.jersey"})
public class SpringStarter
{

    public static void main(String[] args) throws Exception
    {
        SpringApplication.run(SpringStarter.class, args);
    }
}
