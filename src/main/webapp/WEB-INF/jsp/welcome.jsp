<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">

<body>
    <h1>Lista de Eventos do Universo marvel</h1> <br/><br/><br/>
    <c:forEach begin="0" end="${eventos.size() -1}" var="index">
        ${eventos.get(index).getAsJsonObject().get("id").getAsInt()} - ${eventos.get(index).getAsJsonObject().get("title").getAsString()} 
        <a href="/detalhes?itemId=${eventos.get(index).getAsJsonObject().get('id').getAsInt()}">Detalhes</a>
        <br/>
    </c:forEach>
</body>

</html>